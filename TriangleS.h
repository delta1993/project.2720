#ifndef __TRIANGLES_H
#define __TRIANGLES_H

#include "Allegro.h"

///class to initialize Triangles and functions of draw & translate.

class TriangleS : public tRep {
  public:
   TriangleS(double,int); ///constructor
   void draw(); ///draw function
   void trans(Vector); ///translate function
  private:
   double Length; ///initialize sideLength
};
#endif
