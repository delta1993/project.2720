#include "Allegro.h"
#include <iostream>
#include <stdexcept>

Allegro::Allegro()
{
    display = NULL;
    timer = NULL;
    event_queue = NULL;
    framesPerSec = 60;
    looping = true, redraw = false;
    /*  if ((timer = al_create_timer(1.0 / framesPerSec)) == NULL)
		throw std::runtime_error("Cannot create allegro timer");
    if ((event_queue = al_create_event_queue()) == NULL)
    throw std::runtime_error("Cannot create event queue");*/
    al_register_event_source(event_queue, al_get_display_event_source(getAllegroDisplay()));
 
    al_register_event_source(event_queue, al_get_timer_event_source(timer));
  
    al_start_timer(timer);
}

Allegro::~Allegro()
{
    al_destroy_event_queue(event_queue);
    al_destroy_timer(timer);
    al_destroy_display(display);
}

int Allegro::init()
{
    if (!al_init())
    {
        return -1;
    }

    return 0;
}

int Allegro::createWindow(int width, int height)
{
int FPS = framesPerSec;
display = al_create_display(width, height);
//add
/*	al_init();
	
	// if the display cannot be initialized, we should throw an
	// exception. We will deal with exceptons later in the course, so
	// for now, we simply exit
	if ((display = al_create_display(width, height)) == NULL) {
		std::cerr << "Cannot initialize the display" << std::endl;
		exit(1); // non-zero argument means "trouble"
	}

	al_init_primitives_addon();*/
//underline
    
    
	 if (!display)
    {
        al_destroy_display(display);
        return -1;
    }

    timer = al_create_timer(1.0 / FPS);
    if (!timer)
    {
        al_destroy_timer(timer);
        al_destroy_display(display);
        return -1;
    }

    event_queue = al_create_event_queue();
    if (!event_queue)
    {
        al_destroy_event_queue(event_queue);
        al_destroy_timer(timer);
        al_destroy_display(display);
        return -1;
	}

    al_install_keyboard();
    al_init_image_addon();

    al_register_event_source(event_queue, al_get_display_event_source(display));
    al_register_event_source(event_queue, al_get_timer_event_source(timer));
    al_register_event_source(event_queue, al_get_keyboard_event_source());

    player.setBitmap("player.png");

    return 0;
}

void Allegro::updateModel(double dt) {
   for(list<tRep*>::iterator it = fList.begin();
       it != fList.end(); it++)
   {
      if((*it)->origin.y>600) {
	     (*it)->origin.y=0;
	     (*it)->origin.x=rand()%800;
      }
      Vector mv;
      mv.x = dt*((*it)->speed.x);
      mv.y = dt*((*it)->speed.y);

      (*it)->trans(mv);
   }
}

void Allegro::gameLoop()
{
   al_start_timer(timer);
   double crtTime, prevTime = 0;
    while (looping)
    {
       ALLEGRO_EVENT ev;
       al_wait_for_event(event_queue, &ev);
//add
       /* if(ev.type == ALLEGRO_EVENT_TIMER) {
	  crtTime = al_current_time();
	  updateModel(crtTime - prevTime);
	  prevTime = crtTime;
	  // instead of simply calling drawModel() here, we set this flag so that
	  // we redraw only if the event queue is empty; reason: draw is expensive and
	  // we don't want to delay everything too much
	  redraw = true;
       }
       else if (ev.type == ALLEGRO_EVENT_DISPLAY_CLOSE) {
	  break;
       }
       
       if(redraw && al_is_event_queue_empty(event_queue)) {
	  //Draw rain		
	  drawModel();
	  redraw = false;
       }
       */
//underline
       if (ev.type == ALLEGRO_EVENT_KEY_DOWN)
       {
            switch (ev.keyboard.keycode)
            {
            case ALLEGRO_KEY_UP:
                keyboard.key[UP] = true;
                break;
            case ALLEGRO_KEY_LEFT:
                keyboard.key[LEFT] = true;
                break;
            case ALLEGRO_KEY_DOWN:
                keyboard.key[DOWN] = true;
                break;
            case ALLEGRO_KEY_RIGHT:
                keyboard.key[RIGHT] = true;
                break;
            }
        }
	else if (ev.type == ALLEGRO_EVENT_KEY_UP)
        {
            switch (ev.keyboard.keycode)
            {
            case ALLEGRO_KEY_UP:
                keyboard.key[UP] = false;
                break;
            case ALLEGRO_KEY_LEFT:
                keyboard.key[LEFT] = false;
                break;
            case ALLEGRO_KEY_DOWN:
                keyboard.key[DOWN] = false;
                break;
            case ALLEGRO_KEY_RIGHT:
                keyboard.key[RIGHT] = false;
                break;
            }
	    }
        if (ev.type == ALLEGRO_EVENT_TIMER)
        {
            player.doLogic(keyboard);
            redraw = true;
        }
	 else if (ev.type == ALLEGRO_EVENT_DISPLAY_CLOSE)
        {
            looping = false;
	    }
	
	  if (redraw && al_is_event_queue_empty(event_queue))
        {
            redraw = false;

	   al_clear_to_color(al_map_rgb(0, 0, 0));

            // Draw player
	    player.drawT();

	 al_flip_display();
	     }
    }
}
void Allegro::add(tRep* trep) {
   fList.push_back(trep);
}

void Allegro::draw() {
   for(list<tRep*>::iterator it = fList.begin();it != fList.end(); it++)
   {
      (*it)->draw();
   }
}


  
void Allegro::drawModel() {
    al_clear_to_color(al_map_rgb(0,0,0));
    for(list<tRep*>::iterator it = fList.begin();
        it != fList.end(); it++) {
       (*it)->draw();
    }
    al_flip_display();
}






