#include "TriangleS.h"
#include <allegro5/allegro_primitives.h>
#include <cmath>
#include <cstdlib>
///constructor
TriangleS::TriangleS(double sl,int num) : Length(sl) {
   origin.x=rand()%800;
   origin.y=num;
   speed.x = 0;
   speed.y = 100;
}


///draw function
void TriangleS::draw() {
   al_draw_triangle(origin.x, origin.y,
		    origin.x - Length, origin.y + 2*Length,
		    origin.x + Length, origin.y + 2*Length,
		    al_map_rgb(200,200,0),2);
}


///translate function
void TriangleS::trans(Vector v) {
   origin.x += v.x;
   origin.y += v.y;
}
   
