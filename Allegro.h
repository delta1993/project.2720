#ifndef ALLEGRO_H_
#define ALLEGRO_H_

#include <allegro5/allegro.h>
#include <allegro5/allegro_image.h>
#include <allegro5/allegro_primitives.h>
#include <iostream>
#include <list>
#include "Player.h"
#include "Keyboard.h"
using namespace std;

struct Point {
Point() : x(),y(){}
   double x;
   double y;
};

///sturcture to initialize the Vector

struct Vector {
Vector() :x(),y(){}
   double x;
   double y;
};

///class to initialize Shapes and functions of draw & translate.

class tRep {
  public:

   virtual void trans(Vector) = 0;
   virtual void draw() = 0;
   Point origin; ///initialize the origin
   Vector speed; ///initialize the speed
};


class Allegro
{
private:
   ALLEGRO_DISPLAY *display;
   ALLEGRO_TIMER *timer;
    ALLEGRO_EVENT_QUEUE *event_queue;
    int framesPerSec;//the FPS
    Keyboard keyboard;
    Player player;
    bool looping, redraw;
    list<tRep*> fList; 

public:
    Allegro();
    ~Allegro();

    int init();
    int createWindow( int w, int h);

   void draw();
   void add(tRep*);
   void drawModel(); ///draw the model
   void updateModel(double); ///update the model
    void gameLoop();
ALLEGRO_DISPLAY * getAllegroDisplay() const { return display; }
};

#endif
